﻿
namespace Messaging
{
    public interface IMessaging
    {
        void SendSms(string messageText, string countryCode, string to);
    }
}

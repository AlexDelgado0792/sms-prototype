﻿
using System.Configuration;
using Twilio;

namespace Messaging.Twilio
{
    public class TwilioCore
    {
        // Find your Account Sid and Token at twilio.com/console
        private readonly string _accountSid = ConfigurationManager.AppSettings["AccountSid"];
        private readonly string _authToken =ConfigurationManager.AppSettings["AuthToken"];
       
        protected void TwilioContext()
        {
            TwilioClient.Init(_accountSid, _authToken);
        }
    }
}

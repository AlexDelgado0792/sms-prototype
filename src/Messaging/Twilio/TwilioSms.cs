﻿
using System;
using System.Configuration;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Messaging.Twilio
{
    public class TwilioSms : TwilioCore, IMessaging
    {
        //twilio purchase number or test number
        private readonly string _hostPhoneNumber = ConfigurationManager.AppSettings["PhoneNumber"];

        public void SendSms(string messageText, string countryCode, string to)
        {
            TwilioContext();

            var msg = MessageResource.Create(
                body: messageText,
                from: new PhoneNumber(_hostPhoneNumber),
                to: new PhoneNumber(string.Format("+{0}{1}", countryCode, to))
                );
            
            Console.WriteLine(msg.Sid);
        }
    }
}
